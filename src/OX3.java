
import java.util.Scanner;

public class OX3 {

    static final int row = 3, col = 3;
    static int[][] board = new int[row][col];
    static int now_player;
    static int now_row, now_col;

    public static void showWelcome() {
        System.out.println("Welcome To OX Game");
    }

    public static void showBye() {
        System.out.println("Bye Bye...");
    }

    public static void showBoard() {
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                showChoice(board[i][j]);
                if (j != col - 1) {
                    System.out.print("|");
                }
            }
            System.out.println();
            if (i != row - 1) {
                System.out.println("-----------");
            }
        }
        System.out.println();
    }

    public static void showChoice(int choice) {
        final int empty = 0, o = 1, x = 2;
        switch (choice) {
            case empty:
                System.out.print("   ");
                break;
            case o:
                System.out.print(" O ");
                break;
            case x:
                System.out.print(" X ");
                break;
        }
    }

    public static void showTurn(int choice) {
        if (choice == 0) {
            System.out.println("Turn X");
        } else {
            System.out.println("Turn O");
        }
    }

    public static void input(int choice) {
        boolean check = false;
        final int empty = 0;
        Scanner sc = new Scanner(System.in);
        do {
            System.out.print("Input your Row Col :");
            int i = sc.nextInt() - 1, j = sc.nextInt() - 1;
            if (i >= 0 && i < row && j >= 0 && j < col && board[i][j] == empty) {
                now_row = i;
                now_col = j;
                board[now_row][now_col] = choice;
                check = true;
            } else {
                System.out.println("Input try again...");
            }
        } while (!check);
    }

    public static boolean checkWinRow(int chioce, int row) {
        return (board[row][0] == chioce
                && board[row][1] == chioce
                && board[row][2] == chioce);
    }

    public static boolean checkWinCol(int chioce, int col) {
        return (board[0][col] == chioce
                && board[1][col] == chioce
                && board[2][col] == chioce);
    }

    public static boolean checkCross1(int chioce) {
        return (board[0][0] == chioce
                && board[1][1] == chioce
                && board[2][2] == chioce);
    }

    public static boolean checkross2(int chioce) {
        return (board[0][2] == chioce
                && board[1][1] == chioce
                && board[2][0] == chioce);
    }

    public static boolean checkDraw() {
        final int empty = 0;
        for (int i = 0; i < row; i++) {
            for (int j = 0; j < col; j++) {
                if (board[i][j] == empty) {
                    return false;
                }
            }
        }
        return true;
    }

    public static void main(String[] args) {
        showWelcome();
        showBye();
        showBoard();
        showTurn(now_player);
        input(now_player);

    }
}
